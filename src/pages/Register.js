import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom'
import Swal from 'sweetalert2'
import UserContext from '../UserContext'


export default function Register() {

const {user} = useContext(UserContext)

// State hooks to store the values of the input fields
const [firstName, setFirstName] = useState('');
const [lastName, setLastName] = useState('');
const [email, setEmail] = useState('');
const [mobileNumber, setMobileNumber] = useState('');
const [password1, setPassword1] = useState('');
const [password2, setPassword2] = useState('');
// State to determine whether submit button is enabled or not
const [isActive, setIsActive] = useState('');
const [isDuplicate, setIsDuplicate] = useState(false);

// Check if values are successfully binded
	console.log(email);
	console.log(password1);
	console.log(password2);

	function registerUser(e){

		//Prevents page redirection via form submission
		e.preventDefault();

 //        // checkEmailDuplicate(e.email)
 //        console.log(e);

	// 	//Clears the input fields
	// 	setFirstName('');
 //        setLastName('');
 //        setEmail('');
 //        setMobileNumber('');
	// 	setPassword1('');
	// 	setPassword2('');

	// 	alert('Thank you for registering!');
	// }
       
       checkEmailExists(email)

        if (!isDuplicate) {
            
            Swal.fire({
                    title: 'Duplicate email found',
                    icon: 'error',
                    text: 'Please provide a different email'
                })

            setEmail('');
        } else {
            fetch('http://localhost:4000/users/register',{
                method: 'POST',
                headers: {
                    'Content-Type' : 'application/json'
                },
                body: JSON.stringify({
                    firstName: firstName,
                    lastName: lastName,
                    email: email,
                    mobileNo: mobileNumber,
                    password: password1 
                })
            })
            .then(res => res.json())
            .then(data => {
                setFirstName('');
                setLastName('');
                setEmail('');
                setMobileNumber('');
                setPassword1('');
                setPassword2('');

                Swal.fire({
                    title: 'Registration Successful!',
                    icon: 'success',
                    text: 'Welcome to Zuitt'
                })
            })
        }
        
    }    
    
    function checkEmailExists(email){
        fetch('http://localhost:4000/users/checkEmail',{
            method: 'POST',
            headers: {
                'Content-Type' : 'application/json'
            },
            body: JSON.stringify({
                email: email
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log('function checkEmailExists: ' + data);
            setIsDuplicate(data)
            
        })
    }

useEffect(() => {

    // Validation to enable submit button when all fields are populated and both passwords match
    if((email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
        setIsActive(true);
    } else {
        setIsActive(false);
    }

}, [email, password1, password2]);


    return (

    	(user.id !== null)?
    		<Navigate to = 'http://localhost:4000/users/login' />
    	:

        <Form onSubmit={(e)=>registerUser(e)}>

            <Form.Group controlId="userFirstName">
                <Form.Label>First Name</Form.Label>
                <Form.Control 
                    type="name" 
                    placeholder="Enter first name"
                    value={firstName} 
                    onChange={e => setFirstName(e.target.value)} 
                    required
                />
            </Form.Group>

             <Form.Group controlId="userLastName">
                <Form.Label>Last Name</Form.Label>
                <Form.Control 
                    type="name" 
                    placeholder="Enter last name"
                    value={lastName} 
                    onChange={e => setLastName(e.target.value)} 
                    required
                />
            </Form.Group>

            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
	                type="email" 
	                placeholder="Enter email"
	         		value={email} 
	                onChange={e => setEmail(e.target.value)} 
	                required
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group controlId="userMobileNumber">
                <Form.Label>Mobile Number</Form.Label>
                <Form.Control 
                    type="number" 
                    placeholder="Enter Mobile Number"
                    value={mobileNumber} 
                    onChange={e => setMobileNumber(e.target.value)} 
                    required
                />
            </Form.Group>

            <Form.Group controlId="password1">
                <Form.Label>Password</Form.Label>
                <Form.Control 
	                type="password" 
	                placeholder="Password"
	                value={password1} 
	                onChange={e => setPassword1(e.target.value)} 
	                required
                />
            </Form.Group>

            <Form.Group controlId="password2">
                <Form.Label>Verify Password</Form.Label>
                <Form.Control 
	                type="password" 
	                placeholder="Verify Password"
	                value={password2} 
	                onChange={e => setPassword2(e.target.value)} 
	                required
                />
            </Form.Group>

				{/* conditionally render submit button based on isActive state */}
    	    { isActive ? 
    	    	<Button className = "my-3" variant="primary" type="submit" id="submitBtn">
    	    		Submit
    	    	</Button>
    	        : 
    	        <Button className = "my-3" variant="danger" type="submit" id="submitBtn" disabled>
    	        	Submit
    	        </Button>
    	    }

        </Form>
    )

}
